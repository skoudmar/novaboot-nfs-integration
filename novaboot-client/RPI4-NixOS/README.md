# Setup

1. Build the system using the command:

```shell
nix-build ../../nixos-builder/ -A aarch64-linux.novaboot
```

2. Extract the boot directory from the built tarball:

```shell
tar xf rootfs.tar boot
```


# Running the system using the novaboot

1. Upload the root filesystem tar.

```shell
rsync --rsync-path=rsync-nfsroot -L rootfs.tar rpi@server:.
```

2. Extract the filesystem.

```shell
ssh rpi@server untar rootfs.tar
```

3. Start the system.

```shell
boot/boot-rpi4 -i --ssh=rpi@server
```

