# Setup

Create the system by going to the `../../build` directory and building the system using `make`,
or running this here:
```shell
make -C ../../build
```

# Running the system using novaboot

**Start the novaboot-server docker container as explained [here](/README.md).**

To start the system with the root filesystem in RAM drive (uses only TFTP):

```shell
./tftp-boot -i --ssh=novaboot@172.17.0.2
```

To start the system with the root filesystem hosted with NFS:

1. Upload the root filesystem tar.

```shell
rsync --rsync-path=rsync-nfsroot -L rootfs.tar novaboot@172.17.0.2:.
```

2. Extract the filesystem.

```shell
ssh novaboot@172.17.0.2 untar rootfs.tar
```

3. Start the system.

```shell
./nfs-boot -i --ssh=novaboot@172.17.0.2
```

