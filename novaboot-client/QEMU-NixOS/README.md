# Setup

1. Build the system using the command:

```shell
nix-build ../../nixos-builder/ -A aarch64-linux.novaboot
```

2. Extract the boot directory from the built tarball:

```shell
tar xf rootfs.tar boot
```

# Running the system using the novaboot

1. Start the novaboot-server docker container as explained [here](/README.md).

2. Upload the root filesystem tar.

```shell
rsync --rsync-path=rsync-nfsroot -L rootfs.tar novaboot@172.17.0.2:.
```

3. Extract the filesystem.

```shell
ssh novaboot@172.17.0.2 untar rootfs.tar
```

4. Start the system.

```shell
./nfs-boot -i --ssh=novaboot@172.17.0.2 --append=$(cat boot/stage2InitLocation.txt)
```

# Note

The docker does not support creating user namespaces in the container so the UNFS3 is limited
to a single user. This will lead to issues with NixOS.
