# Setup

Create the system by going to the `../../build_rpi4` directory and building the system using `make`,
or running this here:
```shell
make -C ../../build_rpi4
```

# Running the system using novaboot

For RPI there is no server docker image. Please use real server.

To start the system with the root filesystem in RAM drive (uses only TFTP):

```shell
./tftp-boot -i --ssh=rpi@<server>
```

To start the system with the root filesystem hosted with NFS:

1. Upload the root filesystem tar.

```shell
rsync --rsync-path=rsync-nfsroot -L rootfs.tar rpi@<server>:.
```

2. Extract the filesystem.

```shell
ssh rpi@<server> untar rootfs.tar
```

3. Start the system.

```shell
./nfs-boot -i --ssh=rpi@<server>
```