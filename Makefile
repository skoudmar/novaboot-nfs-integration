ADMIN_KEY=admin-key admin-key.pub
SERVER_IMAGES=u-boot.bin
IMAGE_DIR=build/images
CLIENT_IMAGES=Image rootfs.cpio.uboot rootfs.tar

.PHONY: clean copy-images copy-server-images copy-client-images clean-docker-images docker-image run-docker unfs3/unfsd

run-docker:
# removes the container on exit
	docker run -d --rm --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro --name=novaboot-server novaboot-server 
	docker exec -it novaboot-server /bin/bash
	docker stop novaboot-server

$(ADMIN_KEY):
	ssh-keygen -t ed25519 -f admin-key -N "" -C "admin"

docker-image: admin-key.pub
	docker build -t novaboot-server .

clean-docker-images:
	eval 'IMAGES=`docker images -a --filter=dangling=true -q`; if [ $$IMAGES ]; then docker rmi $$IMAGES; fi'

copy-images: copy-server-images copy-client-images

copy-server-images: $(addprefix $(IMAGE_DIR)/,$(SERVER_IMAGES))
	cp $? novaboot-server/

copy-client-images: $(addprefix $(IMAGE_DIR)/,$(CLIENT_IMAGES))
	cp $? novaboot-client/

unfs3-initialized:
	eval 'cd unfs3 && ./bootstrap && ./configure && cd ..'
	touch $@

unfs3/unfsd:
	$(MAKE) -q -C unfs3 || $(MAKE) -C unfs3

unfsd: unfs3-initialized unfs3/unfsd
	cp unfs3/unfsd .

clean:
	rm $(ADMIN_KEY)
