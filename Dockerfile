FROM ubuntu

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Prague

RUN apt-get update && apt-get install -y \
    bsdmainutils \
    libexpect-perl \
    libio-pty-perl \
    libio-stty-perl \
    libtirpc3 \
    make \
    openssh-server \
    perl \
    qemu-system-aarch64 \
    rsync \
    socat

CMD [ "/sbin/init" ]

STOPSIGNAL SIGRTMIN+3

ENV container=docker

# copy admin ssh key for novaboot shell
ADD admin-key.pub /opt/admin-key.pub

# copy and install novaboot
ADD novaboot /opt/novaboot
RUN cd /opt/novaboot/ && make install && make -C server install

# add user novaboot
RUN addgroup novaboot
RUN adduser-novaboot --key /opt/admin-key.pub novaboot --gecos "Novaboot"

ADD novaboot-server/.novaboot-shell /home/novaboot/.novaboot-shell
ADD novaboot-server/.novaboot-nfs /home/novaboot/.novaboot-nfs
ADD novaboot-server/console_cmd.sh /home/novaboot/console_cmd.sh


# add u-boot image
ADD novaboot-server/u-boot.bin /opt/u-boot.bin

# add unfsd executable
ADD unfsd /opt/unfsd
RUN install -m 0755 /opt/unfsd /usr/bin/unfsd

