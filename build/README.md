## Contents

- [`.config`](.config) - config used to build provided images
- [`build/linux-5.10.7/.config`](build/linux-5.10.7/.config) - config for linux kernel
- [`GNUmakefile`](GNUmakefile) - to run buildroot using `make`

## Building Images

```
make
```

### Edit kernel config
```
make linux-menuconfig
```
### Edit other buildroot settings 
```
make menuconfig
```
