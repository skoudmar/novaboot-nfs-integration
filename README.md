# Novaboot NFS integration

## Client systems

### Emulated device in QEMU

- [System by buildroot](/novaboot-client/QEMU-buildroot-system/README.md)

- [NixOS system](/novaboot-client/QEMU-NixOS/README.md)

### Raspberry Pi 4

- [System by buildroot](/novaboot-client/RPI4-buildroot-system/README.md)

- [NixOS system](/novaboot-client/RPI4-NixOS/README.md)

## Server

Setting up the real server is described in [novaboot-server/rpi/README.md](/novaboot-server/rpi/README.md)

Files in [novaboot-server/rpi](/novaboot-server/rpi) directory are used for setting up the real server.

Files in [novaboot-server](/novaboot-server) directory are used by the docker.

### Setting up the docker container

Build server image with tag `novaboot-server`

```shell
make docker-image
```

Run server image in interactive mode without saving on exit

```shell
make
```

### Connecting to the container with SSH

1. Add the ssh key

```shell
ssh-add admin-key
```

2. Connect to the novaboot-shell and start bash.

```shell
ssh novaboot@172.17.0.2 shell
```

## Submodules

### Buildroot

This submodule contains the Buildroot tool for building systems.

Build images in:
- [`build`](build/README.md) directory to create images compatible with QEMU, or

- [`build_rpi4`](build/README.md) directory to create images compatible with Raspberry Pi 4.

Run `make` to create corresponding images.

### NixOS Builder

This submodule contains the cross-system repository for building the Novaboot image of NixOS.

### Novaboot

This submodule contains code for the Novaboot tool.

### Sterm

This submodule contains the source for the serial terminal `sterm` used to communicate with Raspberry Pi over a serial connection.

### UNFS3

This submodule contains the userspace NFS server.

### User namespace creator 

This submodule contains the code for the `userns` tool
