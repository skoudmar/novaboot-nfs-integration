#!/usr/bin/env bash

# run this script as root
if [ "$EUID" -ne 0 ]; then
  echo "Please run as root" >&2
  exit 1
fi

apt-get update && apt-get install -y \
    bsdmainutils \
    git \
    libexpect-perl \
    libio-pty-perl \
    libio-stty-perl \
    libtirpc3 \
    make \
    nfs-kernel-server \
    openssh-server \
    perl \
    rsync \
    socat \
    tftpd-hpa \
    uidmap

# install userns, sterm and unfsd
install -m 0755 -o root -g root userns sterm unfsd /usr/bin/

# install novaboot
git clone https://github.com/skoudmar/novaboot.git

# apply userns patch
echo "$(cd novaboot && git apply ../novaboot-userns.patch)"

# install novaboot
make -C novaboot install
make -C novaboot/server install

# add rpi user
addgroup novaboot
adduser-novaboot --key admin-key.pub rpi --gecos "RaspberryPi"
usermod -aG dialout rpi

# add novaboot-shell configuration files
install -o rpi -g novaboot -m 755 .novaboot-nfs .novaboot-shell /home/rpi