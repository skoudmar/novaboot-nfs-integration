# Prepare files 

1. Set correct IP addresses in `.novaboot-nfs` and `.novaboot-shell`

2. Build sterm, unfsd and user-namespace-creator:
Use `make` in the corresponding directory.

3. Create admin-key
Use `make admin-key` in the top level directory.

# Server setup

## Copy files to the server
```shell
ssh <server> mkdir novaboot-setup
rsync -rL . <server>:novaboot-setup
```

## setup the server
on server run as root
```shell
./server-setup.sh
```