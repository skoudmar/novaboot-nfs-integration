#!/bin/sh

NB_USER=${NB_USER:-$1}

qemu-system-aarch64 \
    -machine virt \
    -m 2G \
    -cpu cortex-a53 \
    -bios /opt/u-boot.bin \
    -nographic \
    -netdev user,id=n1,tftp=/home/novaboot/tftproot/ \
    -device virtio-net,netdev=n1 \
    -smp 1
