# Contents

[`console_cmd.sh`](.console_cmd.sh) - script to start the QEMU

[`.novaboot-shell`](.novaboot-shell) - main configuration file for novaboot

[`.novaboot-nfsroot`](.novaboot-nfsroot) - configuration file for the nfs, existence of this file on server enables support for nfs

[`u-boot.bin`](u-boot.bin) - image of the uboot bootloader<br>
\- place in `/opt/` or update path in `.novaboot-shell`